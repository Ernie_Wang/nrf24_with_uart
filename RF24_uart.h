/**
 * @file RF24_uart.h
 * @brief The definition of class RF24 using Uart. 
 * @author Ernie-Wang<erniewangtw@gmail.com> 
 * @date 2019.07
 * 
 * @par Copyright:
 * Copyright (c) 2019 Ernie-Wang, CYYang
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version. \n\n
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef __RF24_UART_H__
#define __RF24_UART_H__

#include "Arduino.h"


#define RF24_USE_SOFTWARE_SERIAL


#ifdef RF24_USE_SOFTWARE_SERIAL
#include "SoftwareSerial.h"
#endif


/**
 * Provide an easy-to-use way to manipulate RF24_UART. 
 */
class RF24_UART {
 public:

#ifdef RF24_USE_SOFTWARE_SERIAL
    /*
     * Constuctor. 
     *
     * @param uart - an reference of SoftwareSerial object. 
     * @param baud - the buad rate to communicate with RF24_UART(default:9600). 
     *
     * @warning parameter baud depends on the AT firmware. 9600 is an common value. 
     */
    RF24_UART(SoftwareSerial &uart, uint32_t baud = 9600);
#else /* HardwareSerial */
    /*
     * Constuctor. 
     *
     * @param uart - an reference of HardwareSerial object. 
     * @param baud - the buad rate to communicate with RF24_UART(default:9600). 
     *
     * @warning parameter baud depends on the AT firmware. 9600 is an common value. 
     */
    RF24_UART(HardwareSerial &uart, uint32_t baud = 9600);
#endif
    
    
    /** 
     * Verify RF24_UART whether live or not. 
     *
     * Actually, this method will send command "AT?" to RF24_UART and waiting for "OK". 
     * @retval true - alive.
     * @retval false - dead.
     */
    bool kick(void);
    
    /**
     * Set Recieve address connection. 
     * 
     * @param addr - the Address of the target host, five num, 0x??, 0x??, 0x??, 0x??, 0x??.
     * @retval true - success.
     * @retval false - failure.
     */
    bool setRxAdd(const uint8_t *addr, uint32_t len);
    
    /**
     * Set Recieve address connection.
     * 
     * @param addr - the Address of the target host, five num, 0x??, 0x??, 0x??, 0x??, 0x??.
     * @retval true - success.
     * @retval false - failure.
     */
    bool setTxAdd(const uint8_t *addr, uint32_t len);
    
    /**
     * Set the baudrate to comunicate the chip. 
     * 
     * @param baudrate - the baudrate, 1 = 4800, 2 = 9600, 3 = 14400, 4 = 19200, 5 = 38400, 6 = 57600, 7 = 115200.
     * @retval true - success.
     * @retval false - failure.
     */
    bool setBaudRate(uint16_t baudrate);
  
    /**
     * Set the communication rate for rf24. 
     * 
     * @param rate - the sending rate, 1 = 250k, 2 = 1M, 3 = 2M.
     * @retval true - success.
     * @retval false - failure.
     */
    bool setComRate(uint16_t rate);
  
    /**
     * Set the frequency for the comunication. 
     * 
     * @param freq - the frequency desired, with a G at the back. 
     * @retval true - success.
     * @retval false - failure.
     */
    bool setFreq(float freq);
    
    /**
     * Set the CRC for comunication. 
     * 
     * @param crc - set crc, 8 or 16. 
     * @retval true - success.
     * @retval false - failure.
     */
    bool setCRC(uint16_t crc);
    
    /**
     * Send data through rf24. 
     * 
     * @param buffer - the buffer of data to send. 
     * @param len - the length of data to send. 
     * @retval true - success.
     * @retval false - failure.
     */
    bool send(const uint8_t *buffer, uint32_t len);
            
    
    /**
     * Receive data through rf24. 
     *
     * @param buffer - the buffer for storing data. 
     * @param buffer_size - the length of the buffer. 
     * @param timeout - the time waiting data. 
     * @return the length of data received actually. 
     */
    uint32_t recv(uint8_t *buffer, uint32_t buffer_size, uint32_t timeout = 1000);

 private:

    /* 
     * Empty the buffer or UART RX.
     */
    void rx_empty(void);
 
    /* 
     * Recvive data from uart. Return all received data if target found or timeout. 
     */
    String recvString(String target, uint32_t timeout = 1000);
    
    /* 
     * Recvive data from uart. Return all received data if one of target1 and target2 found or timeout. 
     */
    String recvString(String target1, String target2, uint32_t timeout = 1000);
    
    /* 
     * Recvive data from uart. Return all received data if one of target1, target2 and target3 found or timeout. 
     */
    String recvString(String target1, String target2, String target3, uint32_t timeout = 1000);
    
    /* 
     * Recvive data from uart and search first target. Return true if target found, false for timeout.
     */
    bool recvFind(String target, uint32_t timeout = 1000);
    
    /* 
     * Recvive data from uart and search first target and cut out the substring between begin and end(excluding begin and end self). 
     * Return true if target found, false for timeout.
     */
    bool recvFindAndFilter(String target, String begin, String end, String &data, uint32_t timeout = 1000);
    
    /*
     * Receive a package from uart. 
     *
     * @param buffer - the buffer storing data. 
     * @param buffer_size - guess what!
     * @param data_len - the length of data actually received(maybe more than buffer_size, the remained data will be abandoned).
     * @param timeout - the duration waitting data comming. 
     */
    uint32_t recvPkg(uint8_t *buffer, uint32_t buffer_size, uint32_t timeout);
    
    
    bool eAT(void);
    
    bool sATSEND(const uint8_t *buffer, uint16_t len);
    bool sATCRC(uint16_t crc);
    bool sATCOMRATE(uint16_t rate);
    bool sATBAUDRATE(uint16_t baudrate);
    bool sATFREQ(float freq);
    bool sATRXADDR(const uint8_t *addr, uint16_t len);
    bool sATTXADDR(const uint8_t *addr, uint16_t len);
    
    /*
     * +IPD,len:data
     * +IPD,id,len:data
     */
    
#ifdef RF24_USE_SOFTWARE_SERIAL
    SoftwareSerial *m_puart; /* The UART to communicate with RF24_UART */
#else
    HardwareSerial *m_puart; /* The UART to communicate with RF24_UART */
#endif
};

#endif /* #ifndef __RF24_UART_H__ */

